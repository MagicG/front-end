import {Routes} from '@angular/router';

export const APP_ROUTES: Routes = [
  {
    path: 'login',
    loadChildren: './login/login.module#LoginModule',
  },{
    path: 'company',
    loadChildren: './company/company.module#CompanyModule',
  },
  {
    path: 'contact',
    loadChildren: './contact/contact.module#ContactModule',
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];
