import {HttpClient} from '@angular/common/http';
import {HttpConfiguration} from './http.configuration';
import {Injector} from '@angular/core';

export abstract class HttpService extends HttpConfiguration {

  protected abstract injector(): Injector;

  public abstract path(): string;

  public httpClient(): HttpClient {
    return this.injector().get(HttpClient);
  }

  public getUrl(): string {
    return this.getApiAddress() + this.path();
  }

}
