import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LOGIN_ROUTES_CONFIG} from './login-routes';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forChild(LOGIN_ROUTES_CONFIG)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
