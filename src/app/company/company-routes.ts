import {Routes} from '@angular/router';
import {CompanyFormComponent} from './company-form/company-form.component';

export const COMPANY_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: CompanyFormComponent,
    children: [
    ]
  }
];
