import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CompanyRoutingModule} from './company-routing.module';
import {CompanyFormComponent} from './company-form/company-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbAlertModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {CompanyService} from './company.service';

@NgModule({
  declarations: [CompanyFormComponent],
  imports: [
    CommonModule,
    CompanyRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbAlertModule,
    NgbModule
  ],
  providers: [CompanyService]
})
export class CompanyModule {
}
