import {Injectable, Injector} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpService} from '../bootstrap/http.service';
import {Company} from './company-form/body-request/company';


@Injectable({
  providedIn: 'root'
})
export class CompanyService extends HttpService {

  constructor(private _injector: Injector) {
    super();
  }

  path(): string {
    return '/api/users/public/companies';
  }

  public doPost(request: Company): Observable<object> {
    return this.httpClient().post(this.getUrl(), request);
  }

  protected injector(): Injector {
    return this._injector;
  }

}
