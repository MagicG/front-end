import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {emailValidator} from '../../login/login-form/validators/email.validator';
import {LoginHttpService} from '../../login/login-http.service';
import {AuthenticationService} from '../../login/authentication.service';
import {Router} from '@angular/router';
import {CompanyService} from '../company.service';
import {HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-company-form',
  templateUrl: './company-form.component.html',
  styleUrls: ['./company-form.component.scss']
})
export class CompanyFormComponent implements OnDestroy {

  public companyForm: FormGroup;
  public alertMessage: string;
  public thereAreErrors: boolean;
  private readonly passMinLength = 3;
  private companySubscription: Subscription;
  public readonly validDomains = ['gmail', 'hotmail'];

  constructor(private formBuilder: FormBuilder,
              private companyService: CompanyService,
              private authService: AuthenticationService,
              private router: Router) {

    this.companyForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(this.passMinLength)]],
      email: ['', [Validators.required, emailValidator(this.validDomains)]],
      password: ['', [Validators.required, Validators.minLength(this.passMinLength)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(this.passMinLength)]],
    });
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.companySubscription);
  }

  public onSubmit(): void {

    if (this.companyForm.valid) {
      this.companySubscription = this.companyService.doPost(this.companyForm.value).subscribe(
        (response: HttpResponse<any>) => {
          this.router.navigate(['login']);
        },
      );
    }
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

}
